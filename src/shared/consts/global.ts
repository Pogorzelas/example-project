import { ThemeType } from '../types/ThemeType';

export const initialTheme: ThemeType = 'dark';

export const REMEMBERED_USER = 'rememberedUser';
export const SESSION_USER = 'sessionUser';

export const REQUEST_DELAY = 2000;
