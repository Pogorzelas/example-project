import { Store } from "rc-field-form/es/interface";

export type AntdForm<Form> = Form | Store;
