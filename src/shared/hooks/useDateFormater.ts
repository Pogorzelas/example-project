import moment, { Moment } from 'moment';
import { useMemo } from 'react';

const DATE_FORMAT = 'DD/MM/YYYY';

const useDateFormatter = (date: string | Moment) => useMemo(
  () => moment(date).format(DATE_FORMAT),
  [date],
);

export default useDateFormatter;
