import wait from '../utils/wait';
import { Task } from '../interfaces/Task';
import { REQUEST_DELAY } from '../consts/global';
import { v4 } from 'uuid';
import { getAllTasks, saveTaskList } from '../utils/taskUtils';
import { Moment } from 'moment';

interface TaskToCreate {
  name: string;
  description: string;
  startAt: Moment;
  deadline: Moment;
}

const getTaskList = (): Promise<Task[]> => (
  wait(REQUEST_DELAY)
    .then(() => getAllTasks())
);

const postTask = (taskToCreate: TaskToCreate): Promise<Task> => (
  wait(REQUEST_DELAY)
    .then(() => {
      const tasks = getAllTasks();

      const newTask: Task = {
        ...taskToCreate,
        id: v4(),
      };

      tasks.push(newTask);
      saveTaskList(tasks);

      return newTask;
    })
);

const deleteTask = (id: string) => (
  wait(REQUEST_DELAY)
    .then(() => {
      let tasks = getAllTasks();

      tasks = tasks.filter(task => task.id !== id);
      saveTaskList(tasks);

      return Promise.resolve();
    })
);

export {
  postTask,
  getTaskList,
  deleteTask,
};
