import { User } from '../interfaces/UserData';
import wait from '../utils/wait';
import { getLocalStorageItem, setLocalStorageItem, setSessionStorageItem } from '../utils/storageUtils';
import { REQUEST_DELAY, SESSION_USER } from '../consts/global';

const logIn = (email: string, password: string): Promise<User> => (
  wait(REQUEST_DELAY)
    .then(() => {
      const userData = getLocalStorageItem(email);
      const isPasswordMatched = userData?.password === password;

      if (!isPasswordMatched) {
        return Promise.reject();
      }

      setSessionStorageItem(SESSION_USER, email);

      delete userData.password;
      return Promise.resolve(userData);
    })
);

const signUp = (email: string, password: string, nickname: string): Promise<User> => (
  wait(REQUEST_DELAY)
  .then(() => {
    const isUserExists = !!getLocalStorageItem(email);

    if (isUserExists) {
      return Promise.reject();
    }

    const user = {
      email,
      password,
      nickname,
    };

    setLocalStorageItem(email, user);
    setSessionStorageItem(SESSION_USER, email);

    delete user.password;
    return Promise.resolve(user);
  })
);

export {
  logIn,
  signUp,
};
