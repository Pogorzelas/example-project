export enum Routing {
  Home = '/',
  Auth = '/auth',
  Dashboard = '/dashboard',
}
