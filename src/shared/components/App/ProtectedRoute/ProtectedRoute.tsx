import React, { FunctionComponent } from 'react';
import { Route, Redirect } from 'react-router-dom';

interface Props {
  path: string;
  exact: boolean;
  component: FunctionComponent;
  isAccessible?: boolean;
  redirectToWhenInaccessible?: string;
}

const ProtectedRoute = ({
  path,
  exact,
  component: Component,
  redirectToWhenInaccessible,
  isAccessible = true,
}: Props) => (
  <Route
    path={path}
    exact={exact}
  >
    {
      isAccessible
        ? <Component/>
        : <Redirect to={{ pathname: redirectToWhenInaccessible }} />
    }
  </Route>
);

export default ProtectedRoute;
