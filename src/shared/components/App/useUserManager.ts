import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../../store/reducers';
import { useEffect, useState } from 'react';
import { REMEMBERED_USER, SESSION_USER } from '../../consts/global';
import { userActions} from '../../../store/user/actions';
import { getLocalStorageItem, setSessionStorageItem } from '../../utils/storageUtils';

const useUserManager = () => {
  const isLoggedIn = useSelector<AppState, boolean>(state => state.user.isLoggedIn);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(
    () => {
      try {
        const email = getLocalStorageItem(REMEMBERED_USER);

        if (!email) {
          return;
        }

        const user = getLocalStorageItem(email);

        if (!user) {
          return;
        }

        setSessionStorageItem(SESSION_USER, email);
        dispatch(userActions.logIn(user));
      } finally {
        setIsLoading(false);
      }
    },
    [dispatch],
  );

  return {
    isLoading,
    isLoggedIn,
  };
};

export default useUserManager;
