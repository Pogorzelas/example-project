import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Layout from '../Layout/Layout';
import Auth from '../../../views/Auth/Auth';
import { Routing } from '../../enums/Routing';
import Home from '../../../views/Home/Home';
import Dashboard from '../../../views/Dashboard/Dashboard';
import NoMatch from '../../../views/NoMatch/NoMatch';
import useUserManager from './useUserManager';
import ProtectedRoute from './ProtectedRoute/ProtectedRoute';

const App = () => {
  const { isLoggedIn, isLoading } = useUserManager();
  return (
    <BrowserRouter>
      <Layout isLoading={isLoading}>
        <Switch>
          <ProtectedRoute
            path={Routing.Home}
            isAccessible={true}
            component={Home}
            exact={true}
          />
          <ProtectedRoute
            path={Routing.Auth}
            isAccessible={!isLoggedIn}
            redirectToWhenInaccessible={Routing.Home}
            component={Auth}
            exact={true}
          />
          <ProtectedRoute
            path={Routing.Dashboard}
            isAccessible={isLoggedIn}
            redirectToWhenInaccessible={Routing.Auth}
            component={Dashboard}
            exact={true}
          />
          <Route
            path="*"
            component={NoMatch}
          />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
