import React from 'react';
import { Form, Input } from 'antd';
import { Rule } from 'antd/es/form';
import useInputPlaceholder from '../useInputPlaceholder';

interface Props {
  name: string;
  placeholder: string;
  hasFeedback?: boolean;
  rules?: Rule[];
  dependencies?: string[];
}

const PasswordInput = ({
   name,
   placeholder,
   rules = [],
   hasFeedback = false,
   dependencies = [],
}: Props) => {
  const inputPlaceholder = useInputPlaceholder(name, placeholder);
  return (
    <Form.Item
      name={name}
      rules={rules}
      hasFeedback={hasFeedback}
      dependencies={dependencies}
    >
      <Input.Password placeholder={inputPlaceholder}/>
    </Form.Item>
  );
};

export default PasswordInput;
