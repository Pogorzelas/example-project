import { Checkbox, Form } from 'antd';
import React from 'react';
import { Rule } from 'antd/es/form';

interface Props {
  name: string;
  label: string;
  rules?: Rule[];
}

const CheckBoxInput = ({ name, label, rules = [] }: Props) => (
  <Form.Item name={name} valuePropName="checked" rules={rules}>
    <Checkbox>
      {label}
    </Checkbox>
  </Form.Item>
);

export default CheckBoxInput;
