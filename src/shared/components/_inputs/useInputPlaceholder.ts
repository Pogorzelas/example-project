import { useMemo } from 'react';

const useInputPlaceholder = (name: string, placeholder?: string) => useMemo(
  () => {
    if (!placeholder) {
      return name.charAt(0).toUpperCase() + name.substring(1);
    }
    return placeholder;
  },
  [placeholder, name],
);

export default useInputPlaceholder;
