import { Form, Input } from 'antd';
import React from 'react';
import { Rule } from 'antd/es/form';
import useInputPlaceholder from "../useInputPlaceholder";

interface Props {
  name: string;
  placeholder?: string;
  type?: string;
  rules?: Rule[];
}

const TextInput = ({
 name,
  placeholder,
  type = 'text',
  rules = [],
}: Props,
) => {
  const inputPlaceholder = useInputPlaceholder(name, placeholder);
  return (
    <Form.Item name={name} rules={rules}>
      {
        type === 'textarea'
          ? (
            <Input.TextArea
              placeholder={inputPlaceholder}
            />
          )
          : (
            <Input
              type={type}
              placeholder={inputPlaceholder}
            />
          )
      }
    </Form.Item>
  );
};

export default TextInput;
