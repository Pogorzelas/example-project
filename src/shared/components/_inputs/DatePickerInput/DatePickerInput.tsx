import { DatePicker, Form } from 'antd';
import React from 'react';
import { Rule } from 'antd/es/form';
import useInputPlaceholder from '../useInputPlaceholder';

interface Props {
  name: string;
  placeholder?: string;
  rules?: Rule[];
}

const DatePickerInput = ({
   name,
   placeholder,
   rules = [],
}: Props) => {
  const inputPlaceholder = useInputPlaceholder(name, placeholder);
  return (
    <Form.Item name={name} rules={rules}>
      <DatePicker placeholder={inputPlaceholder}/>
    </Form.Item>
  );
};

export default DatePickerInput;
