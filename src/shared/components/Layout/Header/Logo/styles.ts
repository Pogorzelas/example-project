import styled from 'styled-components';
import { Typography } from 'antd';
import { Link } from 'react-router-dom';

export const StyledLink = styled(Link)`
  padding-right: 30px;
`;

export const LogoIcon = styled.img`
  width: 50px;
  height: 50px;
`;

export const AppName = styled(Typography.Text)`
  color: white;
  padding-left: 15px;
  font-size: 18px;
`;
