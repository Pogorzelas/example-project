import React from 'react';
import { AppName, LogoIcon, StyledLink } from './styles';
import logo from '../../../../../assets/social/logo.svg';
import { Routing } from '../../../../enums/Routing';

const Logo = () => (
  <StyledLink to={Routing.Home}>
    <LogoIcon
      src={logo}
      alt={logo}
    />
    <AppName>
      Example Project
    </AppName>
  </StyledLink>
);

export default Logo;
