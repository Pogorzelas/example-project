import styled from 'styled-components';
import { Layout } from 'antd';

export const StyledHeader = styled(Layout.Header)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const NavigationWrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: inherit;
`;
