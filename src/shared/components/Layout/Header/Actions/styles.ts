import styled from 'styled-components';
import { Button } from 'antd';

export const StyledButton = styled(Button)`
  margin-left: 10px;
`;
