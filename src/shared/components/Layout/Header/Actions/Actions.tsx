import React from 'react';
import useActions from './useActions';
import { StyledButton } from './styles';
import {Link} from "react-router-dom";
import {Routing} from "../../../../enums/Routing";

const Actions = () => {
  const { isLoggedIn, handleLogOut } = useActions();
  return (
    <>
      {
        isLoggedIn && (
          <Link to={Routing.Auth}>
            <StyledButton
              onClick={handleLogOut}
              ghost={true}
              type="link"
              size="small"
            >
              Log Out
            </StyledButton>
          </Link>
        )
      }
    </>
  );
};

export default Actions;
