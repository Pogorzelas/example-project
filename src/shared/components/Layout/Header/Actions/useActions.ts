import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../../../../store/reducers';
import { userActions} from '../../../../../store/user/actions';

const useActions = () => {
  const isLoggedIn = useSelector<AppState, boolean>(state => state.user.isLoggedIn);
  const dispatch = useDispatch();

  const handleLogOut = () => {
    dispatch(userActions.logOut());
  };

  return {
    isLoggedIn,
    handleLogOut,
  };
};

export default useActions;
