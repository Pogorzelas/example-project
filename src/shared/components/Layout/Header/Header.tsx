import React from 'react';
import { NavigationWrapper, StyledHeader } from './styles';
import Navigation from './Navigation/Navigation';
import Logo from './Logo/Logo';
import Actions from './Actions/Actions';

const Header = () => (
  <StyledHeader>
    <NavigationWrapper>
      <Logo/>
      <Navigation/>
    </NavigationWrapper>
    <Actions/>
  </StyledHeader>
);

export default Header;
