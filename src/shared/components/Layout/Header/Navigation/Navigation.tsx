import React from 'react';
import { StyledItem, StyledMenu } from './styles';
import { Link } from 'react-router-dom';
import useNavigation from './useNavigation';

const Navigation = () => {
  const { selectedKey, navigationList } = useNavigation();
  return (
    <StyledMenu
      mode="horizontal"
      selectedKeys={[selectedKey]}
      theme='dark'
    >
      {
        navigationList.map(({ path, name }) => (
          <StyledItem
            key={path}
          >
            <Link to={path}>
              {name}
            </Link>
          </StyledItem>
        ))
      }
    </StyledMenu>
  );
};

export default Navigation;
