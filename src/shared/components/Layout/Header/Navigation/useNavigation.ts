import { useLocation } from 'react-router-dom';
import { useMemo } from 'react';
import navLinks from './navLinks';
import { useSelector } from 'react-redux';
import { AppState } from '../../../../../store/reducers';
import { Routing } from '../../../../enums/Routing';

const useNavigation = () => {
  const isLoggedIn = useSelector<AppState, boolean>(state => state.user.isLoggedIn);
  const { pathname } = useLocation();

  const navigationList = useMemo(
    () => navLinks.filter((navLink) => {
      switch (navLink.path) {
        case Routing.Auth:
          return !isLoggedIn;
        case Routing.Dashboard:
          return isLoggedIn;
        default:
          return true;
      }
    }),
    [isLoggedIn],
  );

  return {
    navigationList,
    selectedKey: pathname,
  };
};

export default useNavigation;
