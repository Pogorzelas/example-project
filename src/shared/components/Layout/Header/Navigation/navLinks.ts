import { Routing } from '../../../../enums/Routing';

const navLinks = [
  {
    path: Routing.Home,
    name: 'Home',
  },
  {
    path: Routing.Auth,
    name: 'Log In / Sign Up',
  },
  {
    path: Routing.Dashboard,
    name: 'Dashboard',
  },
];

export default navLinks;
