import styled from 'styled-components';
import { Menu } from 'antd';

export const StyledMenu = styled(Menu)`
  color: white;
  display: flex;
  justify-content: center;
`;

export const StyledItem = styled(Menu.Item)`
`;
