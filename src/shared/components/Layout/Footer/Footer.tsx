import React from 'react';
import { StyledText, StyledFooter, Link } from './styles';

const AUTHOR_LINK = 'https://www.flaticon.com/authors/freepik';
const SITE_LINK = 'https://www.flaticon.com/';

const Footer = () => (
  <StyledFooter>
    © 2020 Example Project. All right reversed.
    <StyledText>
      Icons made by
      <Link
        href={AUTHOR_LINK}
        title="Freepik"
        target="_blank"
      >
        Freepik
      </Link>
      from
      <Link
        href={SITE_LINK}
        title="Flaticon"
        target="_blank"
      >
        www.flaticon.com
      </Link>
    </StyledText>
  </StyledFooter>
);

export default Footer;
