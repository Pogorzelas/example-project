import styled from 'styled-components';
import { Layout, Typography } from 'antd';

export const StyledFooter = styled(Layout.Footer)`
  display: flex;
  flex-direction: column;
`;

export const StyledText = styled(Typography.Text)`
  font-size: 11px;
`;

export const Link = styled.a`
  padding: 5px;
  color: inherit;
`;
