import styled from 'styled-components';
import { Layout } from 'antd';

export const StyledLayout = styled(Layout)`
  display: flex;
  min-height: 100vh;
`;
