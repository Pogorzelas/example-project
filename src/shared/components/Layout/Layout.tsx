import React, { ReactNode } from 'react';
import Footer from './Footer/Footer';
import Header from './Header/Header';
import { StyledLayout } from './styles';

interface Props {
  children: ReactNode;
  isLoading: boolean;
}

const Layout = ({ children, isLoading }: Props) => (
  <StyledLayout>
    <Header/>
    {
      isLoading
        ? <></>
        : children
    }
    <Footer/>
  </StyledLayout>
);

export default Layout;
