import React, { ReactNode } from 'react';
import { StyledContent } from './styles';
import useDocumentName from './useDocumentName';

interface Props {
  children: ReactNode;
  styleLess?: boolean;
}

const View = ({ children, styleLess = false }: Props) => {
  useDocumentName();
  return (
    <StyledContent styleless={styleLess.toString()}>
      {
        children
      }
    </StyledContent>
  );
};

export default View;
