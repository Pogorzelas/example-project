import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { Routing } from '../../../enums/Routing';

const APP_NAME = 'Example Project';

const useDocumentName = () => {
  const { pathname } = useLocation();
  useEffect(
    () => {
      switch (pathname) {
        case Routing.Home:
          document.title = `${APP_NAME} - Home`;
          break;
        case Routing.Auth:
          document.title = `${APP_NAME} - Join Us`;
          break;
        case Routing.Dashboard:
          document.title = `${APP_NAME} - Manage Tasks`;
          break;
        default:
          document.title = `${APP_NAME} - Not Found`;
      }
    },
    [pathname],
  );
};

export default useDocumentName;
