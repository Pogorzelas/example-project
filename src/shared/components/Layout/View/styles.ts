import styled from 'styled-components';
import { Layout } from 'antd';

export const StyledContent = styled(Layout.Content)`
  ${props => props.styleless === 'false' && `
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  `}
`;
