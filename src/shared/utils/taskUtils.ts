import { getLocalStorageItem, getSessionStorageItem, setLocalStorageItem } from './storageUtils';
import { SESSION_USER } from '../consts/global';
import { Task } from '../interfaces/Task';

const getTaskKey = (): string => {
  const email = getSessionStorageItem(SESSION_USER);
  return `${email}_tasks`;
};

export const getAllTasks = (): Task[] => {
  const tasksKey = getTaskKey();
  return getLocalStorageItem(tasksKey) || [];
};

export const saveTaskList = (tasks: Task[]) => {
  const tasksKey = getTaskKey();
  setLocalStorageItem(tasksKey, tasks);
};
