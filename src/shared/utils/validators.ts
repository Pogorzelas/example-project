import { RuleRender } from 'rc-field-form/lib/interface';

export const validatePasswords: RuleRender = ({ getFieldValue }) => ({
  validator(rule, value) {
    if (!value || getFieldValue('password') === value) {
      return Promise.resolve();
    }
    return Promise.reject('The two passwords that you entered do not match!');
  },
});

export const checkboxValidate = (errorMessage: string): RuleRender => () => ({
  validator: (rule, value) =>
    value
      ? Promise.resolve()
      : Promise.reject(errorMessage),
});
