export const setLocalStorageItem = (name: string, dataToSave: any) => {
  localStorage.setItem(name, JSON.stringify(dataToSave));
};

export const  getLocalStorageItem = (name: string) => {
  const item = localStorage.getItem(name);
  return item
    ? JSON.parse(item)
    : null;
};

export const removeLocalStorageItem = (name: string) => {
  localStorage.removeItem(name);
};

export const setSessionStorageItem = (name: string, dataToSave: any) => {
  sessionStorage.setItem(name, JSON.stringify(dataToSave));
};

export const  getSessionStorageItem = (name: string) => {
  const item = sessionStorage.getItem(name);
  return item
    ? JSON.parse(item)
    : null;
};

export const removeSessionStorageItem = (name: string) => {
  sessionStorage.removeItem(name);
};
