import { Moment } from 'moment';

export interface Task {
  id: string;
  name: string;
  description: string;
  startAt: Moment;
  deadline: Moment;
}
