import View from '../../shared/components/Layout/View/View';
import React from 'react';
import { Link } from 'react-router-dom';
import { Routing } from '../../shared/enums/Routing';

const NoMatch = () => (
  <View>
    Page Not Found
    <Link to={Routing.Home}>Go Home</Link>
  </View>
);

export default NoMatch;
