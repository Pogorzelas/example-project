import LoginForm from './LoginForm/LoginForm';
import { Tabs } from 'antd';
import React from 'react';
import SignUpForm from './SignUpForm/SignUpForm';
import { StyledCard } from './styles';

const { TabPane } = Tabs;

const AuthCard = () => {
  return (
    <StyledCard>
      <Tabs
        defaultActiveKey="1"
      >
        <TabPane tab="Log In" key="1">
          <LoginForm/>
        </TabPane>
        <TabPane tab="Sign Up" key="2">
          <SignUpForm/>
        </TabPane>
      </Tabs>
    </StyledCard>
  );
};

export default AuthCard;
