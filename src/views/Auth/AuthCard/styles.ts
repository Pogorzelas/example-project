import styled from 'styled-components';
import { Card } from 'antd';

export const StyledCard = styled(Card)`
  width: 370px;
  .ant-card-body {
    padding-top: 12px;
  }
`;
