import styled from 'styled-components';
import { Typography } from 'antd';

export const StyledErrorText = styled(Typography.Text)`
  padding-left: 10px;
`;
