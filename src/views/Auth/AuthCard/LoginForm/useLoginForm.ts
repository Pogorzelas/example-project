import { useEffect, useMemo, useRef, useState } from 'react';
import { REMEMBERED_USER } from '../../../../shared/consts/global';
import { logIn } from '../../../../shared/apiMocks/auth';
import { useHistory } from 'react-router-dom';
import { Routing } from '../../../../shared/enums/Routing';
import { useDispatch } from 'react-redux';
import { userActions} from '../../../../store/user/actions';
import { setLocalStorageItem } from '../../../../shared/utils/storageUtils';
import {AntdForm} from "../../../../shared/types/AntdForm";

interface FormData {
  email: string;
  password: string;
  isRemember: false;
}

const useLoginForm = () => {
  const [errorMassage, setErrorMassage] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  const isSubscribed = useRef(true);
  const initialValues = useMemo(
    () => ({
      email: '',
      password:  '',
      isRemember: false,
    }),
    [],
  );

  // Primitive log in mechanism
  const handleFinish = ({ email, password, isRemember }: AntdForm<FormData>) => {
    setIsLoading(true);
    logIn(email, password)
      .then((userData) => {
        dispatch(userActions.logIn(userData));
        history.push(Routing.Dashboard);
      })
      .catch(() => {
        if (isSubscribed.current) {
          setErrorMassage('Email or password are incorrect');
        }
      })
      .finally(() => {
        if (isSubscribed.current) {
          setIsLoading(false);
        }
      });

    if (isRemember) {
      setLocalStorageItem(REMEMBERED_USER, email);
    }
  };

  useEffect(
    () => {
      const onUnmount = () => {
        isSubscribed.current = false;
      };
      return onUnmount;
    },
    [],
  );

  return {
    isLoading,
    errorMassage,
    initialValues,
    handleFinish,
  };
};

export default useLoginForm;
