import React from 'react';
import { Button, Form } from 'antd';
import useLoginForm from './useLoginForm';
import TextInput from '../../../../shared/components/_inputs/TextInput/TextInput';
import CheckBoxInput from '../../../../shared/components/_inputs/CheckBoxInput/CheckBoxInput';
import PasswordInput from '../../../../shared/components/_inputs/PasswordInput/PasswordInput';
import { StyledErrorText } from './styles';

const LoginForm = () => {
  const { handleFinish, initialValues, isLoading, errorMassage } = useLoginForm();
  return (
    <Form
      onFinish={handleFinish}
      initialValues={initialValues}
    >
      <TextInput
        name="email"
        type="email"
        placeholder="Email"
      />
      <PasswordInput
        name="password"
        placeholder="Password"
      />
      <CheckBoxInput
        name="isRemember"
        label="Remember me"
      />
      <Button
        htmlType="submit"
        loading={isLoading}
      >
        Submit
      </Button>
      <StyledErrorText type="danger">
        {errorMassage}
      </StyledErrorText>
    </Form>
  );
};

export default LoginForm;
