import { useEffect, useMemo, useRef, useState } from 'react';
import { signUp } from '../../../../shared/apiMocks/auth';
import { useHistory } from 'react-router-dom';
import { Routing } from '../../../../shared/enums/Routing';
import { useDispatch } from 'react-redux';
import { userActions} from '../../../../store/user/actions';
import {AntdForm} from "../../../../shared/types/AntdForm";

interface FormData {
  email: string;
  password: string;
  nickname: string;
  termsAccepted: boolean;
}

const useSignUpForm = () => {
  const [errorMessage, setErrorMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  const isSubscribed = useRef(true);
  const initialValues = useMemo(
    () => ({
      email: '',
      password: '',
      nickname: '',
      termsAccepted: false,
    }),
    [],
  );

  // Primitive log in mechanism
  const handleFinish = ({ email, password, nickname }: AntdForm<FormData>) => {
    setIsLoading(true);
    signUp(email, password, nickname)
      .then((userData) => {
        dispatch(userActions.logIn(userData));
        history.push(Routing.Dashboard);
      })
      .catch(() => {
        if (isSubscribed.current) {
          setErrorMessage('User already exists');
        }
      })
      .finally(() => {
        if (isSubscribed.current) {
          setIsLoading(false);
        }
      });
  };

  useEffect(
    () => {
      const onUnmount = () => {
        isSubscribed.current = false;
      };
      return onUnmount;
    },
    [],
  );

  return {
    isLoading,
    errorMessage,
    initialValues,
    handleFinish,
  };
};

export default useSignUpForm;
