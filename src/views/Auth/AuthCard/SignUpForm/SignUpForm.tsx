import React from 'react';
import { Button, Form } from 'antd';
import useSignUpForm from './useSignUpForm';
import TextInput from '../../../../shared/components/_inputs/TextInput/TextInput';
import CheckBoxInput from '../../../../shared/components/_inputs/CheckBoxInput/CheckBoxInput';
import PasswordInput from '../../../../shared/components/_inputs/PasswordInput/PasswordInput';
import { checkboxValidate, validatePasswords } from '../../../../shared/utils/validators';
import { StyledErrorText } from './styles';

const SignUpForm = () => {
  const { handleFinish, initialValues, isLoading, errorMessage } = useSignUpForm();
  return (
    <Form
      onFinish={handleFinish}
      initialValues={initialValues}
    >
      <TextInput
        name="email"
        type="email"
        placeholder="Email"
        rules={[
          {
            type: 'email',
            message: 'The input is not valid E-mail!',
          },
          {
            required: true,
            message: 'Please input your E-mail!',
          },
        ]}
      />
      <PasswordInput
        name="password"
        placeholder="Password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
        hasFeedback={true}
      />
      <PasswordInput
        name="confirmPassword"
        placeholder="Confirm Password"
        dependencies={['password']}
        hasFeedback={true}
        rules={[
          {
            required: true,
            message: 'Please confirm your password!',
          },
          validatePasswords,
        ]}
      />
      <TextInput
        name="nickname"
        placeholder="Nickname"
      />
      <CheckBoxInput
        name="termsAccepted"
        label="I accept the Terms of Service & Privacy Policy"
        rules={[
          checkboxValidate('Should accept agreement'),
        ]}
      />
      <Button
        htmlType="submit"
        loading={isLoading}
      >
        Submit
      </Button>
      <StyledErrorText type="danger">
        {errorMessage}
      </StyledErrorText>
    </Form>
  );
};

export default SignUpForm;
