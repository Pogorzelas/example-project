import React, { FunctionComponent } from 'react';
import View from '../../shared/components/Layout/View/View';
import AuthCard from './AuthCard/AuthCard';

const Auth: FunctionComponent = () => (
  <View>
    <AuthCard/>
  </View>
);

export default Auth;
