import React from 'react';
import View from '../../shared/components/Layout/View/View';
import TaskList from './TaskList/TaskList';
import UserSection from './UserSection/UserSection';

const Dashboard = () => (
  <View>
    <UserSection/>
    <TaskList/>
  </View>
);

export default Dashboard;
