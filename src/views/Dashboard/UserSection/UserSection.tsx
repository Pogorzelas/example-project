import useUserData from './useUserData';
import React, { FC } from 'react';

const UserSection: FC = () => {
  const { nickname } = useUserData();

  return <p>Hello {nickname}!</p>;
};

export default UserSection;
