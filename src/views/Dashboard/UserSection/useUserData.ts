import { useSelector } from 'react-redux';
import { AppState } from '../../../store/reducers';
import { User } from '../../../shared/interfaces/UserData';

const useUserData = () => {
  const { nickname } = useSelector<AppState, User>(state => state.user);

  return {
    nickname,
  };
};

export default useUserData;
