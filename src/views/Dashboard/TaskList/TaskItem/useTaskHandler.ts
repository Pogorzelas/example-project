import { useDispatch } from 'react-redux';
import { deleteTask } from '../../../../shared/apiMocks/tasks';
import { taskActions} from '../../../../store/task/actions';
import { useState } from 'react';

const useTaskHandler = (id: string) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const handleDelete = () => {
    setIsLoading(true);
    deleteTask(id)
      .then(() => {
        dispatch(taskActions.removeTask(id));
      });
  };

  return {
    isLoading,
    handleDelete,
  };
};

export default useTaskHandler;
