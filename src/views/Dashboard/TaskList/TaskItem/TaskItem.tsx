import { Button, List } from 'antd';
import React from 'react';
import useTaskHandler from './useTaskHandler';
import { DateWrapper, Icon, StyledDeleteIcon, StyledSpin } from './styles';
import useDateFormatter from '../../../../shared/hooks/useDateFormater';
import CalendarIcon from '../../../../assets/social/calendar.svg';
import StartIcon from '../../../../assets/social/start.svg';
import {Moment} from "moment";

interface Props {
  id: string;
  name: string;
  description: string;
  startAt: Moment;
  deadline: Moment;
}

const TaskItem = ({
  id,
  name,
  description,
  startAt,
  deadline,
}: Props) => {
  const { handleDelete, isLoading } = useTaskHandler(id);
  const startDate = useDateFormatter(startAt);
  const finishDate = useDateFormatter(deadline);
  return (
    <List.Item
      extra={(
        <>
          <Button
            icon={<StyledDeleteIcon/>}
            type="link"
            onClick={handleDelete}
          />
          <StyledSpin
            isloading={isLoading.toString()}
          />
        </>
      )}
    >
      <List.Item.Meta
        title={name}
        description={(
          <DateWrapper>
          <span>
            <Icon src={StartIcon} title="start"/>{startDate}
          </span>
            <span>
            <Icon src={CalendarIcon} title="deadline"/>{finishDate}
          </span>
          </DateWrapper>
        )}
      />
      {description}
    </List.Item>
  );
};

export default TaskItem;
