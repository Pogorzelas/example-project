import styled from 'styled-components';
import { Spin } from 'antd';
import { DeleteFilled } from '@ant-design/icons';

export const DateWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Icon = styled.img`
  width: 25px;
  height: 25px;
  margin: 5px;
`;

export const StyledDeleteIcon = styled(DeleteFilled)`
  color: red;
`;

export const StyledSpin = styled(Spin)`
  ${props => props.isloading === 'false' && `
    visibility: hidden;
  `}
`;
