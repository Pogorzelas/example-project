import { useDispatch, useSelector } from 'react-redux';
import { taskActions } from '../../../store/task/actions';
import React, { useEffect, useRef, useState } from 'react';
import { getTaskList } from '../../../shared/apiMocks/tasks';
import { AppState } from '../../../store/reducers';
import { Task } from '../../../shared/interfaces/Task';
import TaskItem from './TaskItem/TaskItem';

const useTaskManager = () => {
  const dispatch = useDispatch();
  const taskList = useSelector<AppState, Task[]>(state => state.task.list);
  const isSubscribed = useRef(true);
  const [isLoading, setIsLoading] = useState(false);

  const toggleModal = () => {
    dispatch(taskActions.toggleModal());
  };

  const renderItem = (props: Task) => (
    <TaskItem
      {...props}
    />
  );

  useEffect(
    () => {
      setIsLoading(true);
      getTaskList()
        .then((tasks) => {
          dispatch(taskActions.setList(tasks));
        })
        .finally(() => {
          if (isSubscribed.current) {
            setIsLoading(false);
          }
        });

      const onUnmount = () => {
        isSubscribed.current = false;
      };

      return onUnmount;
    },
    [dispatch],
  );

  return {
    renderItem,
    isLoading,
    taskList,
    toggleModal,
  };
};

export default useTaskManager;
