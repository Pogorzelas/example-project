import React from 'react';
import { Button, List } from 'antd';
import { StyledCard, StyledIcon } from './styles';
import TaskModal from './TaskModal/TaskModal';
import useTaskManager from './useTaskManager';

const TaskList = () => {
  const { toggleModal, taskList, renderItem, isLoading } = useTaskManager();
  return (
    <StyledCard
      title="What to do?"
      extra={(
        <Button
          type="link"
          icon={<StyledIcon/>}
          onClick={toggleModal}
        />
      )}
    >
      <TaskModal/>
      <List
        rowKey="id"
        size="small"
        dataSource={taskList}
        itemLayout="vertical"
        renderItem={renderItem}
        loading={isLoading}
      />
    </StyledCard>
  );
};

export default TaskList;
