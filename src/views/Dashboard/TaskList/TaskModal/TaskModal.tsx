import React from 'react';
import { Button, Form, Modal } from 'antd';
import useTaskModalManager from './useTaskModalManager';
import TextInput from '../../../../shared/components/_inputs/TextInput/TextInput';
import DatePickerInput from '../../../../shared/components/_inputs/DatePickerInput/DatePickerInput';
import { DateWrapper } from './styles';

const TaskModal = () => {
  const { isOpened, isLoading, toggleModal, handleFinish } =  useTaskModalManager();
  return (
    <Modal
      title="Add task"
      visible={isOpened}
      onCancel={toggleModal}
      footer={null}
      destroyOnClose={true}
    >
      <Form
        onFinish={handleFinish}
      >
        <TextInput
          name="name"
          rules={[
            {
              required: true,
              message: 'Name your task',
            },
          ]}
        />
        <TextInput
          name="description"
          type="textarea"
          rules={[
            {
              required: true,
              message: 'Boring... add some content',
            },
          ]}
        />
        <DateWrapper>
          <DatePickerInput
            name="startAt"
            placeholder="start"
            rules={[
              {
                required: true,
                message: 'When will you start?',
              },
            ]}
          />
          <DatePickerInput
            name="deadline"
            rules={[
              {
                required: true,
                message: 'Choose deadline',
              },
            ]}
          />
        </DateWrapper>
        <Button htmlType="submit" loading={isLoading}>
          Submit
        </Button>
      </Form>
    </Modal>
  );
};

export default TaskModal;
