import styled from 'styled-components';

export const DateWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;
