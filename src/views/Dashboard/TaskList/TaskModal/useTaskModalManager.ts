import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../../../store/reducers';
import { taskActions } from '../../../../store/task/actions';
import { postTask } from '../../../../shared/apiMocks/tasks';
import { useState } from 'react';
import { Moment } from 'moment';
import {AntdForm} from "../../../../shared/types/AntdForm";

interface FormData {
  name: string;
  description: string;
  startAt: Moment;
  deadline: Moment;
}

const useTaskModalManager = () => {
  const dispatch = useDispatch();
  const isOpened = useSelector<AppState, boolean>(state => state.task.isModalOpened);
  const [isLoading, setIsLoading] = useState(false);

  const toggleModal = () => {
    if (!isLoading) {
      dispatch(taskActions.toggleModal());
    }
  };

  const handleFinish = (values: AntdForm<FormData>) => {
    setIsLoading(true);
    postTask(values as FormData)
      .then((newTask) => {
        dispatch(taskActions.addTask(newTask));
        toggleModal();
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return {
    isLoading,
    isOpened,
    toggleModal,
    handleFinish,
  };
};

export default useTaskModalManager;
