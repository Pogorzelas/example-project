import styled from 'styled-components';
import { PlusCircleFilled } from '@ant-design/icons';
import { Card } from 'antd';

export const StyledCard = styled(Card)`
  width: 500px;
`;

export const StyledIcon = styled(PlusCircleFilled)`
  font-size: 25px;
`;
