import styled from 'styled-components';
import { Card } from 'antd';

export const CardWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: -50px;
`;

export const StyledCard = styled(Card)`
  .ant-card-body {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
