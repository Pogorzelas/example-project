import React from 'react';
import {CardWrapper, StyledCard} from './styles';
import {Button} from 'antd';
import {Routing} from '../../../shared/enums/Routing';
import {Link} from 'react-router-dom';
import {useSelector} from "react-redux";
import {AppState} from "../../../store/reducers";

const OfferCard = () => {
  const isLoggedIn = useSelector<AppState, boolean>(state => state.user.isLoggedIn);
  return (
    <CardWrapper>
      <StyledCard hoverable={true}>
        <h2>
          Are you interested?
        </h2>
        <Link to={isLoggedIn ? Routing.Dashboard : Routing.Auth}>
          <Button
            type="primary"
            size="large"
          >
            {
              isLoggedIn
                ? 'Check your tasks'
                : ' Join Us!'
            }
          </Button>
        </Link>
      </StyledCard>
    </CardWrapper>
  );
};

export default OfferCard;
