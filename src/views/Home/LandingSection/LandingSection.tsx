import React from 'react';
import {Container, Description, LandingHeader, StyledIcon, Wrapper, StyledText} from './styles';
import LandingImage from '../../../assets/social/landing-image.png';

const LandingSection = () => (
  <Container>
    <Wrapper>
      <Description>
        <LandingHeader>
          Lorem ipsum
        </LandingHeader>
        <StyledText>
          In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate
          the visual form of a document or a typeface without relying on meaningful content.
          Lorem ipsum may be used before final copy is available,
          but it may also be used to temporarily replace copy in a process called greeking,
          which allows designers to consider form without the meaning of the text influencing the design.
        </StyledText>
      </Description>
      <StyledIcon
        src={LandingImage}
        alt="landing-desktop"
      />
    </Wrapper>
  </Container>
);

export default LandingSection;
