import styled from 'styled-components';
import {Typography} from "antd";

export const LandingHeader = styled.h1`
  font-size: 48px;
  color: white;
`;

export const StyledText = styled(Typography.Text)`
  font-size: 20px;
  color: white;
`;

export const Container = styled.div`
  background: linear-gradient(
    125deg,
    rgba(0,41,81,0.9) 0%,
    rgba(0,121,237, 1) 100%
  );
  height: 70vh;
  clip-path: ellipse(95% 75% at 50% 23%);
  font-family: "Source Sans Pro", sans-serif;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const Description = styled.div`
  color: white;
  padding: 30px;
  width: 500px;
`;

export const StyledIcon = styled.img`
  width: 600px;
  height: 500px;
`;
