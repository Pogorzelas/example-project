import React from 'react';
import View from '../../shared/components/Layout/View/View';
import LandingSection from './LandingSection/LandingSection';
import OfferCard from './OfferCard/OfferCard';

const Home = () => (
  <View styleLess={true}>
    <LandingSection/>
    <OfferCard/>
  </View>
);

export default Home;
