import { combineReducers } from 'redux';
import { UserState } from './user/state';
import user from './user/reducer';
import { TaskState } from './task/state';
import task from './task/reducer';

export interface AppState {
  user: UserState;
  task: TaskState;
}

export default combineReducers({
  user,
  task,
});
