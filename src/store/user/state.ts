export interface UserState {
  isLoggedIn: boolean;
  nickname: string;
  email: string;
}

export const initialState: UserState = {
  isLoggedIn: false,
  nickname: '',
  email: '',
};
