import { initialState, UserState } from './state';
import { UserAction } from './actions';

const userReducer = (
  state: UserState = initialState,
  action: UserAction,
) => {
  switch (action.type) {
    case 'User.logIn':
      const userData = action.payload;
      return {
        ...state,
        ...userData,
        isLoggedIn: true,
      };
    case 'User.logOut':
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

export default userReducer;
