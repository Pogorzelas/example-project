import { Action } from '../../shared/interfaces/Action';
import { User } from '../../shared/interfaces/UserData';
import { removeLocalStorageItem, removeSessionStorageItem } from '../../shared/utils/storageUtils';
import { REMEMBERED_USER } from '../../shared/consts/global';

export type UserAction =
  | Action<
    'User.logIn',
    User
  >
  | Action<'User.logOut'>;

export const userActions = {
  logIn: (userData: User): UserAction => {
    return {
      type: 'User.logIn',
      payload: userData,
    };
  },
  logOut: (): UserAction => {
    removeLocalStorageItem(REMEMBERED_USER);
    removeSessionStorageItem(REMEMBERED_USER);
    return {
      type: 'User.logOut',
      payload: undefined,
    };
  },
};
