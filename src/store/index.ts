import { createStore } from 'redux';
import combinedReducers from './reducers';

function configureStore() {
  return createStore(
    combinedReducers,
  );
}

export default configureStore();
