import { Action } from '../../shared/interfaces/Action';
import { Task } from '../../shared/interfaces/Task';

export type TaskAction =
  | Action<'Task.setList', {
    list: Task[];
  }>
  | Action<'Task.addTask', {
    task: Task;
  }>
  | Action<'Task.removeTask', {
    id: string;
  }>
  | Action<'Task.editTask', {
    task: Task;
  }>
  | Action<'Task.toggleModal'>;

export const taskActions = {
  setList: (list: Task[]): TaskAction => {
    return {
      type: 'Task.setList',
      payload: {
        list,
      },
    };
  },

  addTask: (task: Task): TaskAction => {
    return {
      type: 'Task.addTask',
      payload: {
        task,
      },
    };
  },

  removeTask: (id: string): TaskAction => {
    return {
      type: 'Task.removeTask',
      payload: {
        id,
      },
    };
  },


  toggleModal: (): TaskAction => {
    return {
      type: 'Task.toggleModal',
      payload: undefined,
    };
  },
};
