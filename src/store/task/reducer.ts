import { initialState, TaskState } from './state';
import { TaskAction } from './actions';

const taskReducer = (
  state: TaskState = initialState,
  action: TaskAction,
) => {
  switch (action.type) {
    case 'Task.setList':
      const { list } = action.payload;
      return {
        ...state,
        list,
      };
    case 'Task.addTask': {
      const { task } = action.payload;
      return {
        ...state,
        list: [...state.list, task],
      };
    }
    case 'Task.removeTask': {
      const { id } = action.payload;
      return {
        ...state,
        list: state.list.filter(item => item.id !== id),
      };
    }
    case 'Task.toggleModal': {
      return {
        ...state,
        isModalOpened: !state.isModalOpened,
      };
    }
    default:
      return state;
  }
};

export default taskReducer;
