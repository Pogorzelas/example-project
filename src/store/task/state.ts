import { Task } from '../../shared/interfaces/Task';

export interface TaskState {
  list: Task[];
  isModalOpened: boolean;
}

export const initialState: TaskState = {
  list: [],
  isModalOpened: false,
};
